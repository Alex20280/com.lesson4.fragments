package layouts.sourceit.com.comlesson4fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyGeniousFragment extends Fragment {

    private static final String KEY = "key"; //ключ для хранения входящих параметров

    /**
     * Метод создает новый фрагмент и устанавливает в качестве аргументов text
     * @param text - входящий параметр
     * @return готовый врагмент
     */
    public static MyGeniousFragment newInstance(String text) {
        Bundle args = new Bundle();
        args.putString(KEY, text);
        MyGeniousFragment fragment = new MyGeniousFragment();
        fragment.setArguments(args);
        return fragment;
    }

    String value;

    @BindView(R.id.text)
    TextView text;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Если присутствуют аргументы, считываем их и записываем в переменную
        if (getArguments() != null && getArguments().containsKey(KEY)) {
            value = getArguments().getString(KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //создаем view на основе разметки в xml
        View root = inflater.inflate(R.layout.fragment_my, container, false);
        //тот же findById для text. Биндим вьюхи
        ButterKnife.bind(this, root);
        //устанавливаем текст
        text.setText(value);
        //возвращаем разметку xml
        return root;
    }














}